var express =  require('express');
    compression = require('compression'),
    bodyparser = require('body-parser');

const Sequelize = require('sequelize');
const connection = new Sequelize('company_details', 'root', 'admin', {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },

});

app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}));

app.set('port', process.env.PORT || 5000);

app.use(compression());

var Employee = connection.define('employee',{
   name:{
       type: Sequelize.STRING
   },
    companyName:{
       type: Sequelize.STRING
    },
    dateOfBirth:{
       type: Sequelize.STRING
    },
    email:{
       type: Sequelize.STRING
    },
    designation:{
       type: Sequelize.STRING
    }
});

var Company =  connection.define('company',{
   companyName:{
       type: Sequelize.STRING
   },
   address:{
        type: Sequelize.STRING
   }

});

app.post('/createNewEmployee', function (req, res) {
var message = 'Company Not found';
    Company.findOne({ where: {companyName: req.body.companyName} }).then(companyObject => {
        if(companyObject){
            Employee.create({
                name: req.body.name,
                companyName: req.body.companyName,
                dateOfBirth: req.body.dateOfBirth,
                email: req.body.email,
                designation: req.body.designation,
                companyId: companyObject.id
            });
            message = "Created New Employee";
        }
        res.send(message);
    });

});


app.post('/createNewCompany', function (req, res) {
    Company.create({
       companyName: req.body.companyName,
       address: req.body.address
    }).then(data => {
        var emp = req.body.employees;
        var i = 0;
        for (; i < emp.length; i++) {
            Employee.create({
                name: emp[i].name,
                companyName: emp[i].companyName,
                dateOfBirth: emp[i].dateOfBirth,
                email: emp[i].email,
                designation: emp[i].designation,
                companyId: data.id
            });
        }
        if(emp.length == 0){
            Company.findAll({
                include:[
                    {model: Employee}
                ]
            }).then(data => {
                res.send(data);
            });
        }
    });
});

connection.sync({
});

Company.hasMany(Employee, {foreignKey:'companyId'});

app.listen(app.get('port'), function () {
    console.log('Express server listening on port http://localhost:' + app.get('port'));
});
